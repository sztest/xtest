local function isspace(pos)
	local d = pos:nodedef()
	return d and (not d.walkable or d.climbable) 
end

local function tell(user, ...)
	local t = sz_table:new({ ... })
	for k, v in pairs(t) do
		t[k] = tostring(v)
	end
	local name = user:get_player_name()
	local msg = t:concat(" ")
	minetest.chat_send_player(name, msg, false)
	print(name .. ": " .. msg)
end

xt_player.registertool({
	name = "tool_teleport",
	description = "Teleport",
	inventory_image = "xt_player_tool_teleport.png",
	on_use = function(stack, user)
		if not user or not user:is_player() then return end
		local pos = sz_pos:new(user:getpos())
		pos.y = pos.y + 1.5
		local dir = sz_pos:new(user:get_look_dir())
		local fx = { }
		for i = 1, 256 do
			local newpos = pos:add(dir)
			local def = newpos:nodedef()
			if def and def.walkable then
				pos = newpos:round():scan_flood(5, function(p)
					if isspace(p) and isspace(p:add(sz_pos.dirs.u)) then
						return p
					end
				end)
				if pos then
					tell(user, "Teleporting to", tostring(pos))
					user:setpos(pos:add(sz_pos.dirs.d:scale(0.48)))
					local vmin = dir:add(sz_pos:xyz(-0.5, -0.5, -0.5))
					local vmax = dir:add(sz_pos:xyz(0.5, 0.5, 0.5))
					for i, v in ipairs(fx) do
						minetest.add_particlespawner(5, 0.1,
						v, v, vmin, vmax, sz_pos.zero,
						sz_pos.zero, 1, 5, 1, 1,
						false, "xt_player_tool_teleport.png")
					end
					return
				else
					tell(user, "Not enough space around target.")
				end
			end
			pos = newpos
			if i % 4 == 1 then
				table.insert(fx, pos)
			end
		end
		tell(user, "No target within range.")
	end
})
