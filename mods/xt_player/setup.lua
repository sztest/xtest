-- Players' bodies are non-corporeal, so they don't get damaged.
minetest.setting_set("enable_damage", "false")

-- Initial setup on joining players.
minetest.register_on_joinplayer(function(player)
	-- Setup player visuals.
	player:set_properties({
		visual = "upright_sprite",
		visual_size = { x = 1, y = 2 },
		textures = { "xt_player_front.png", "xt_player_back.png" },
	})

	-- Disable player's inventory menu; eventually we may add
	-- other functionality back to this menu.
	player:set_inventory_formspec("")

	-- Load all initial tools.
	xt_player.loadtools(player)
end)

-- Fix up players on each tick.
minetest.register_globalstep(function(dtime)
	for k, v in pairs(minetest.get_connected_players()) do
		-- Reset health and disable drowning for all players.
		v:set_hp(20)
		v:set_breath(11)

		-- Reset any toolbar tools that may have gotten moved.
		xt_player.loadtools(v)
	end
end)
