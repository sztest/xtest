xt_player.tools = sz_table:new()

-- Register a new tool for player use, and register it to
-- be loaded automatically into all players' toolbars.
function xt_player.registertool(def)
	-- Auto-prefix mod name.
	if def.name:gsub(":", "") == def.name then
		def.name = minetest.get_current_modname()
			.. ":" .. def.name
	end

	-- Set some default values.
	def.stack_max = def.stack_max or 1
	def.range = def.range or 0
	def.on_drop = def.on_drop or function() end

	-- Register the tool with minetest.
	minetest.register_craftitem(def.name, def)

	-- Register the tool, if not already done,
	-- to load into the player's toolbar.
	for i, v in ipairs(xt_player.tools) do
		if v == def.name then return end
	end
	xt_player.tools:insert(def.name)
end

-- Ensure that the given player has all registered tools
-- loaded into the toolbar.
function xt_player.loadtools(player)
	local inv = player:get_inventory()
	for i, v in pairs(xt_player.tools) do
		local stack = inv:get_stack('main', i)
		if not stack or stack:get_name() ~= v then
			inv:set_stack('main', i, v)
		end
	end
end
