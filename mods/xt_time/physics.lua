local function modent(name, func)
	local ent = minetest.registered_entities[name]
	if ent then ent = func(ent) or ent end
	minetest.register_entity(":" .. name, ent)
end

function xt_time.scale_physics(ent, getgrav)
	getgrav = getgrav or function() return 1 end
	local oldact = ent.on_activate
	ent.on_activate = function(self, data)
		if data and type(data) == "string"
			and data:sub(0, 9) == "return { " then
			data = minetest.deserialize(data)
		else
			data = { base = data }
		end
		if data.vel then
			self.truevel = data.vel
			self.object:setvelocity(sz_pos:new(data.vel)
				:scale(xt_time:speed_get()))
		end
		return oldact and oldact(self, data.base)
	end
	local oldsave = ent.get_staticdata
	ent.get_staticdata = function(self)
		local data = { base = oldsave and oldsave(self) }
		if xt_time.speed_get() > 0 then
			local v = self.object:getvelocity()
			data.vel = sz_pos:new(v):scale(1 / xt_time:speed_get())
		else
			data.vel = self.truevel
		end
		return minetest.serialize(data)
	end
	local oldstep = ent.on_step
	ent.on_step = function(self, dtime)
		self.oldtime = self.oldtime or 1
		local function helper(...)
			local ts = xt_time.speed_get()
			if self.oldtime ~= ts then
				if self.oldtime == 0 then
					if self.truevel then
						local v = self.truevel
						self.object:setvelocity(sz_pos:new(v)
							:scale(ts))
					end
				else
					local v = self.object:getvelocity()
					v = sz_pos:new(v):scale(1 / self.oldtime)
					self.truevel = v
					self.object:setvelocity(v:scale(ts))
				end
				self.oldtime = ts
			end
			self.object:setacceleration(sz_pos.dirs.d:scale(
				10 * getgrav(self) * ts))
			return ...
		end
		return helper(oldstep and oldstep(self, dtime))
	end
	return ent
end

modent("__builtin:falling_node", xt_time.scale_physics)
modent("__builtin:item", function(ent)
	xt_time.scale_physics(ent, function(self)
		if self.physical_state then
			return 1
		else
			return 0
		end
	end)
end)
