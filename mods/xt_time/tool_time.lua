xt_player.registertool({
	name = "tool_time",
	description = "Time Control",
	inventory_image = "xt_time_tool_time.png",
	on_use = function(stack, user)
		if not user or not user:is_player() then return end
		local n = user:get_player_name()
		local sp = xt_time.player_speed_get(n)
		if sp > 0 then
			if sp < 16 then sp = sp * 2 end
		else
			sp = 0.25
		end
		xt_time.player_speed_set(n, sp)
		minetest.chat_send_player(n, "Time speed limit set to " .. sp, false)
	end
})
