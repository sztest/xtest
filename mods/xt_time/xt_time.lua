local daytime = 0
minetest.after(0, function()
	daytime = minetest.get_timeofday()
end)

local time_speed = 0

function xt_time.speed_get()
	return time_speed
end

local laststatus = ""
function xt_time.speed_set(val, limiters)
	val = val or 0
	local changed = val ~= time_speed
	time_speed = val

	local dayspeed = time_speed * 72
	if minetest.setting_get("time_speed") ~= dayspeed then
		minetest.setting_set("time_speed", dayspeed)
	end

	local status = "Time speed set to " .. val .. "x"
	if limiters and limiters[1] then
		table.sort(limiters)
		status = status .. " limited by " .. table.concat(limiters, ", ")
	end
	if laststatus ~= status then
		minetest.chat_send_all(status)
		print(status)
		laststatus = status
	end

	return changed
end

local player_allowed_speeds = { }
function xt_time.player_speed_get(name)
	return player_allowed_speeds[name] or 0
end
function xt_time.player_speed_set(name, val)
	player_allowed_speeds[name] = val
end
minetest.register_on_joinplayer(function(player)
	player_allowed_speeds[player:get_player_name()] = 0
end)
minetest.register_on_leaveplayer(function(player)
	player_allowed_speeds[player:get_player_name()] = nil
end)

minetest.register_globalstep(function(dtime)
	local speed = nil
	local limiters = { }
	for k, v in pairs(minetest.get_connected_players()) do
		local n = v:get_player_name()
		local w = v:get_wielded_item()
		local sp = 0
		if not w or w:get_name() ~= "xt_time:tool_time" then
			player_allowed_speeds[n] = 0
		else
			sp = player_allowed_speeds[n]
		end
		if not speed or speed > sp then
			speed = sp
			limiters = { n }
		elseif speed == sp then
			table.insert(limiters, n)
		end
	end
	speed = speed or 0
	local speedchange = xt_time.speed_set(speed, limiters)
	daytime = (daytime or 0) + dtime * speed / 86400
	daytime = daytime - math.floor(daytime)
	if speedchange or (speed > 0) and (math.abs(minetest.get_timeofday()
		- daytime) > (speed / 17280)) then
		minetest.set_timeofday(daytime)
	end
end)
