local selected_agents = { }

-- Add an agent to the player's selection.
function xt_agent.select_add(player, agent)
	if type(agent) == "userdata" then
		agent = agent:get_luaentity()
	end
	selected_agents[player] = selected_agents[player] or sz_table:new()
	selected_agents[player][agent] = true
end

-- Clear agents from player selection.
function xt_agent.select_clear(player)
	selected_agents[player] = nil
end

-- Get the agents a player has selected.
function xt_agent.select_get(player)
	return selected_agents[player] or sz_table:new()
end

local function playerclr(player)
	xt_agent.select_clear(player:get_player_name())
end
minetest.register_on_joinplayer(playerclr)
minetest.register_on_leaveplayer(playerclr)

local function isspace(pos)
	local d = pos:nodedef()
	return d and (not d.walkable or d.climbable) 
end
function xt_agent.can_fit(pos)
	pos = pos and sz_pos:new(pos)
	if not pos or not isspace(pos)
		or not isspace(pos:add(sz_pos.dirs.u)) then
		return
	end
	return true
end
local function issolid(pos)
	local d = pos:nodedef()
	return d and (d.walkable and not d.climable)
end
function xt_agent.can_stand(pos)
	pos = pos and sz_pos:new(pos)
	return pos and xt_agent.can_fit(pos)
		and issolid(pos:add(sz_pos.dirs.d))
end
