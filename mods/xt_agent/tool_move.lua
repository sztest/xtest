local function isspace(pos)
	local d = pos:nodedef()
	return d and (not d.walkable or d.climbable) 
end

xt_player.registertool({
	name = "tool_move",
	description = "Move Agent",
	inventory_image = "xt_agent_tool_move.png",
	range = 4,
	on_use = function(stack, user, pointed)
		if not user or not user:is_player() then return end
		local name = user:get_player_name()

		if pointed.type == "object" then
			if not user:get_player_control().sneak then
				xt_agent.select_clear(name)
			end
			xt_agent.select_add(name, pointed.ref)
			minetest.chat_send_player(name,
				tostring(xt_agent.select_get(name):count())
				.. " agent(s) selected.", false)
		else if pointed.type == "node" then
			local wps, moved = 0, 0
			local sel = xt_agent.select_get(name):copy()
			local total = sel:count()
			sz_pos:new(pointed.above):scan_flood(5, function(p)
				if not isspace(p) then return false end
				if not xt_agent.can_stand(p) then return end
				for k, v in pairs(sel) do
					if not user:get_player_control().sneak then
						k.data.move = sz_table:new()
					end
					local src = k.data.move and k.data.move[#k.data.move]
						or k:feetpos()
					local path = minetest.find_path(src, p, 16, 1, 5, "A*")
					if path and path[1] then
						local t = k.data.move or sz_table:new()
						wps = wps + #path
						for i, v in ipairs(path) do
							t:insert(sz_pos:new(v))
						end
						p.w = true
						t:insert(p)
						k.data.owner = name
						sel[k] = nil
						moved = moved + 1
					end
					break
				end
			end)
			minetest.chat_send_player(name,
				tostring(wps) .. " waypoint(s) added to "
				.. tostring(moved) .. " agent(s), "
				.. tostring(total - moved) .. " failed to find paths.",
				false)
			end
		end
	end
})
