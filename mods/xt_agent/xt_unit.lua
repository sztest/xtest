function xt_unit:set_staticdata(data)
	if data and type(data) == "string" and data:sub(1, 7) == "return " then
		data = minetest.deserialize(data)
	end
	if data and type(data) == "table" then
		data = sz_table:new(data)
	end
	self.data = data or self.data
end

function xt_unit:get_staticdata()
	return minetest.serialize(self.data)
end

function xt_unit:base_get()
	return sz_pos:new(self.object:getpos())
		:add(sz_pos:xyz(0, -self.unitsize / 2 - 0.25))
		:round()
end

function xt_unit:base_set(pos)
	if self:base_get():eq(pos) then return end
	self.object:moveto(sz_pos:new(pos)
		:add(sz_pos:xyz(0, 0.5, 0)))
end

local function ispassable(pos)
	local d = pos:nodedef()
	return d and (not d.walkable or d.climbable) 
end
local function isbreathable(pos)
	local d = pos:nodedef()
	return d and (not d.walkable or d.climbable) and not d.drowning
end
local function issolid(pos)
	local d = pos:nodedef()
	return d and (d.walkable or d.climbable)
end
local function isclimbable(pos)
	local d = pos:nodedef()
	return d and d.climbable
end

local function hasfloor(self, pos)
	for x = math.floor(-self.unitsize.x / 2 + 0.5),
		math.ceiling(self.unitsize.x / 2 - 0.5) do
		for z = math.floor(-self.unitsize.x / 2 + 0.5),
			math.ceiling(self.unitsize.x / 2 - 0.5) do
			if issolid(sz_pos:xyz(x, -1, z):add(pos)) then
				return true
			end
			if isclimbable(sz_pos:xyz(x, 0, z):add(pos)) then
				return true
			end
		end
	end
end
local function hasspace(self, pos)
	for x = math.floor(-self.unitsize.x / 2 + 0.5),
		math.ceiling(self.unitsize.x / 2 - 0.5) do
		for z = math.floor(-self.unitsize.x / 2 + 0.5),
			math.ceiling(self.unitsize.x / 2 - 0.5) do
			for y = 0, math.ceiling(self.unitsize.y - 1) do
				local func = y >= 0 and isbreathable or ispassable
				if not func(sz_pos:xyz(x, y, z):add(pos)) then
					return
				end
			end
		end
	end
	return true
end
local function hasnocollide(self, pos)
	local center = pos:add(sz_pos:xyz(0, self.unitsize.y / 2, 0))
	local amin = pos:sub(sz_pos:xyz(self.unitsize.x / 2, 0, self.unitsize.x / 2))
	local amax = pos:add(sz_pos:xyz(self.unitsize.x / 2, self.unitsize.y, self.unitsize.x / 2))
	for i, v in pairs(minetest.get_objects_inside_radius(center,
		sz_pos:xyz(self.unitsize.x, self.unitsize.y, self.unitsize.x):len() * 2)) do
		local size = (v and v:get_luaentity() or { }).unitsize
		if size then
			size = sz_pos:xyz(size.x / 2, size.y / 2, size.x / 2)
			local pos = sz_pos:new(v:getpos())
			local bmin = pos:sub(size)
			local bmax = pos:add(size)
			local xmin = amin.x > bmin.x and amin.x or bmin.x
			local xmax = amax.x < bmax.x and amax.x or bmax.x
			if xmin < xmax then
				local ymin = amin.y > bmin.y and amin.y or bmin.y
				local ymax = amax.y < bmax.y and amax.y or bmax.y
				if ymin < ymax then
					local zmin = amin.z > bmin.z and amin.z or bmin.z
					local zmax = amax.z < bmax.z and amax.z or bmax.z
					if zmin < zmax then
						return
					end
				end
			end
		end
	end
	return true
end

function xt_unit:can_fit(pos)
	pos = pos and sz_pos:new(pos)
	return hasspace(self, pos) and hasnocollide(self, pos)
end
function xt_agent:can_stand(pos)
	pos = pos and sz_pos:new(pos)
	return pos and self:can_fit(pos) and hasfloor(self, pos)
end
function xt_agent:find_floor(pos, maxfall)
	pos = pos and sz_pos:new(pos)
	if not hasspace(self, pos) then
		pos = pos:copy()
		pos.y = pos.y + 1
		if self:can_stand(pos) then
			return pos
		end
		return
	end
	pos = pos:copy()
	for dy = 0, -math.floor(maxfall) do
		if hasfloor(self, pos) and hasnocollide(self, pos) then
			return pos
		end
		pos.y = pos.y - 1
	end
end

function xt_unit:register()
	if self.name:gsub(":", "") == self.name then
		self.name = minetest.get_current_modname() .. ":" .. self.name
	end
	self.unitsize = self.unitsize or { x = 1, y = 2 }
	self.initial_properties = sz_table:new(self.initial_properties or { }):merge({
		physical = true,
		collisionbox = {
			-self.unitsize.x / 2,
			-self.unitsize.y / 2,
			-self.unitsize.x / 2,
			self.unitsize.x / 2,
			self.unitsize.y / 2,
			self.unitsize.x / 2
		},
		visual_size = self.unitsize,
		visual = "upright_sprite",
		textures = {
			self.name:gsub("%W", "_") .. "_front.png",
			self.name:gsub("%W", "_") .. "_back.png"
		},
		is_visible = true
	}
	self.on_activate = self.on_activate or self.set_staticdata
	minetest.register_entity(self.name, self)
end
