minetest.register_entity('xt_agent:agent', xt_time.scale_physics({
	initial_properties = {
		physical = true,
		collisionbox = { -0.5, -1, -0.5, 0.5, 1, 0.5 },
		visual = 'upright_sprite',
		visual_size = { x = 1, y = 2 },
		textures = { 'xt_agent_front.png', 'xt_agent_back.png' },
		is_visible = true,
		makes_footstep_sound = true,
		automatic_rotate = false,
		automatic_face_movement_dir = 0
	},
	on_activate = function(self, data)
		self.data = sz_table:new(data and minetest.deserialize(data))
		self.data.move = sz_table:new(self.data.move)
	end,
	get_staticdata = function(self)
		return self.data:serialize()
	end,
	feetpos = function(self)
		local feet = sz_pos:new(self.object:getpos()):add(sz_pos.dirs.d:scale(0.75)):round()
		local def = feet:nodedef()
		if def and def.walkable then return feet:add(sz_pos.dirs.u) end
		return feet
	end,
	feetmove = function(self, pos)
		if not self:feetpos():eq(pos) then
			self.object:moveto(sz_pos:new(pos):add(sz_pos.dirs.u:scale(0.5)):round(), true)
		end
	end,
	notify = function(self, ...)
		local t = sz_table:new({ ... })
		for k, v in pairs(t) do
			t[k] = tostring(v)
		end
		local msg = t:concat(" ")
		if self.data.owner then
			minetest.chat_send_player(self.data.owner, msg, false)
		else
			minetest.chat_send_all(msg)
		end
		print(msg)
	end,
	on_step = function(self, dtime)
		self.data.time = (self.data.time or 0) + dtime * xt_time.speed_get()
		local pos = self:feetpos()
		local moved
		local movecost = 1
		while xt_agent.can_stand(pos) and self.data.time >= movecost do
			local dest = self.data.move and self.data.move[1]
			if not dest then break end
			if pos:eq(dest) then
				self.data.move:remove(1)
			else
				local dir = sz_pos:new(dest):sub(pos)
				dir.y = 0
				dir = pos:add(dir:dir27())
				if xt_agent.can_fit(dir) then
					pos = dir
					self.data.time = self.data.time - movecost
				else
					dir = dir:add(sz_pos.dirs.u)
					if xt_agent.can_fit(dir) then
						pos = dir
						self.data.time = self.data.time - movecost
					else
						local newpath = minetest.find_path(
							sz_pos:new(self:feetpos()),
							dest, 16, 1, 5, "A*")
						if newpath and newpath[1] then
							local new = #newpath
							while not self.data.move[1].w do
								self.data.move:remove(1)
							end
							newpath = sz_table:new(newpath)
							for i, v in ipairs(self.data.move) do
								newpath:insert(v)
							end
							self.data.move = newpath
							self:notify("Agent got rerouted from " .. tostring(pos)
								.. " via " .. new .. " total " .. #self.data.move)
						elseif self.data.owner then
							self:notify("Agent got stuck at " .. tostring(pos)
								.. " to " .. tostring(dir))
							self.data.move = nil
						end
					end
				end
			end
		end
		self:feetmove(pos)
		if (not self.data.move or not self.data.move[1]) then
			self.data.time = 0
		end
	end
}))
